---
layout: page
title: Acknowledgements
permalink: /shoshin/acknowledgements
display: true
---

The CSS styling and underlining I took from the [Tufte CSS project](https://github.com/edwardtufte/tufte-css), while the overall designs and beautiful English fonts were originally created by [Edward Tufte](https://www.edwardtufte.com/tufte/), himself. The Japanese font used for regular text is available as part of the [Google Noto Fonts](https://www.google.com/get/noto/), and the font for the title is available at [OpenType.jp](https://opentype.jp/kouzangyousho.htm). Mathematical text is rendered using [MathJax](https://www.mathjax.org/) and
[LaTeX](https://www.latex-project.org/), and markdown for everything from headers, links, and lists to footnotes is rendered using [Kramdown](https://kramdown.gettalong.org/index.html).

This is a [Jekyll](https://jekyllrb.com/) site hosted on [GitLab Pages](https://about.gitlab.com/features/pages/). You can view the source for this blog [here](https://gitlab.com/dempedempe/dempedempe.gitlab.io/).
