---
layout: page
title: About
permalink: /shoshin/about
display: true
---

<div class="epigraph">
  <blockquote>
    <p>In the beginner's mind there are many possibilities; in the expert's mind there are few.</p>
    <footer>Shunryu Suzuki, <cite>Zen Mind, Beginner's Mind</cite></footer>
  </blockquote>
</div>

{%
   include margin_note_with_image.html
   note='Me eating a durian for the first time in Bangkok.'
   src='/img/eating_durian.jpg'
   alt='Image of me eating a durian for the first time in Bangkok.'
%}

I'm Chris Dempewolf, [yet another](https://en.wikipedia.org/wiki/Yet_another) software engineer (YASE) based in Seattle. I set up this blog primarily to explore my thoughts and interests via writing. I don't really know what I know until I write it, so this blog is meant to serve as my light in the dark. My interests err on the side of breadth rather than depth, so expect posts on a wide variety of subjects scientific, opinionated, speculative, or plain ole expository.

This site's title, <span lang="ja">{{ site.title | escape }}</span> (pronounced sho-sheen), is a Zen concept roughly translated as "beginner's mind."  It implies curiosity, openness, and approaching things without preconceptions.  It is the goal of this blog to explore many new and interesting topics - always through the eyes of a beginner.

Thanks for visiting.  If you like what you see, subscribe to my [RSS feed](/feed.xml). You can reach me at <a href="mailto:chrisdempewolf@protonmail.com">chrisdempewolf@protonmail.com</a>.
